<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_whereami" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#reviewing"/>
    <link type="next" xref="howto_flat_review"/>
    <title type="sort">3. Onde Estou</title>
    <desc>Aprendendo sobre sua localização</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Onde Estou</title>
  <p>Além dos comandos específicos para a leitura da barra de título e da barra de estado, o Orca fornece dois comandos sensíveis ao contexto do tipo Onde Estou: Onde Estou básico e o Onde Estou detalhado.. O Onde Estou básico é implementado para todos os objetos. O Onde Estou detalhado é implementado somente para aqueles objetos para os quais você deseja saber significativa quantidade de informação, entretanto, provavelmente não deseja saber o tempo todo.</p>
  <p>A melhor maneira de familiarizar-se com quê o comando "Onde Estou" apresentará é tentar (usar) os <link xref="commands_where_am_i">comandos Onde Estou</link>. No entanto, para lhe proporcionar uma melhor compreensão da natureza sensível ao contexto do recurso "Onde Estou" do <app>Orca</app>, considere o seguinte:</p>
  <p>Para a maioria dos widgets, você será informado pelo menos sobre o rótulo e/ou nome, o tipo ou função do widget e a tecla mnemônica e/ou aceleradora, se existirem. Além disso:</p>
  <list>
    <item>
      <p>Se o controle for texto e você executar um Onde Estou básico, você será informado sobre a linha atual se nenhum texto for selecionado. Se o texto for selecionado, no entanto, um Onde Estou básico lhe dirá qual texto está selecionado. Um Onde Estou detalhado no texto também incluirá os atributos do texto.</p>
    </item>
    <item>
      <p>Se o controle puder ser marcado, como é o caso das caixas de seleção e botões de opção, o estado marcado será incluído.</p>
    </item>
    <item>
      <p>Se o controle for uma lista ou objeto semelhante a uma lista, como uma caixa de combinação, um grupo de botões de opção ou uma lista de guias de página, a posição do item atual será incluída.</p>
    </item>
    <item>
      <p>Se o controle for hierárquico, como uma árvore, e você estiver em um nó expansível, será informado se esse nó está expandido ou não. E se for expandido, você também será informado de quantos elementos filhos ele contém. Além disso, o nível de aninhamento também será fornecido.</p>
    </item>
    <item>
      <p>Se o controle for uma barra de progresso ou um controle deslizante, você será informado sobre a porcentagem atual.</p>
    </item>
    <item>
      <p>Se o controle for um ícone dentro de um grupo de ícones, um Onde Estou básico incluirá o objeto em que você está, o item em que você está e o número de itens selecionados. Em um Onde Estou detalhado, você também será informado sobre quais itens são selecionados.</p>
    </item>
    <item>
      <p>Se você estiver em um link, o tipo do link (mesmo site, site diferente, link de FTP, etc.) será incluído.</p>
    </item>
    <item>
      <p>Se você estiver em uma célula de uma tabela, as coordenadas dessa célula e os cabeçalhos da célula será incluídos.</p>
    </item>
    <item>
      <p>Se você estiver no corretor ortográfico de um aplicativo em que o <app>Orca</app> fornece suporte aprimorado, um Onde Estou básico repetirá o erro respeitando suas <link xref="preferences_spellcheck">preferências de verificação ortográfica</link>. Um Onde Estou detalhado fará com que o <app>Orca</app> apresente todos os detalhes do erro.</p>
    </item>
  </list>
  <p>E assim por diante. Novamente, o objetivo do Onde Estou do <app>Orca</app> é fornecer os detalhes que você provavelmente está interessado em saber sobre o objeto em que está atualmente. Para experimentar o Onde Estou, consulte o lista de <link xref="commands_where_am_i">Comandos Onde Estou</link>.</p>
</page>
