<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="introduction" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#getting_started"/>
    <link type="next" xref="howto_setting_up_orca"/>
    <title type="sort">1. Bem-vindo ao Orca</title>
    <desc>Introdução ao leitor de telas <app>Orca</app></desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Bem-vindo ao Orca</title>
  <p>O <app>Orca</app> é um leitor de telas livre, de código aberto, flexível, e extensível que fornece acesso ao ambiente de trabalho gráfico através de fala e braile atualizável.</p>
  <p>O <app>Orca</app> funciona com aplicativos e kits de ferramentas que oferecem suporte à Assistive Technology Service Provider Interface (Interface do Provedor de Serviços de Tecnologia Assistiva, AT-SPI em inglês), que é a principal infraestrutura de tecnologia assistiva para Linux e Solaris. Aplicativos e kits de ferramentas que suportam o AT-SPI incluem Gtk+, Qt, Java Swing, LibreOffice, Gecko, WebKitGtk e Chrome/Chromium.</p>
  <section id="launching">
    <title>Executando o <app>Orca</app></title>
    <p>Para executar o <app>Orca</app>:</p>
    <list>
      <item>
        <p>O método para configurar o <app>Orca</app> para ser iniciado automaticamente como seu leitor de tela preferido dependerá de qual ambiente de desktop você usa.</p>
      </item>
      <item>
        <p>Para alternar o <app>Orca</app> em ligado e desligado no GNOME, pressione as teclas <keyseq><key>Super</key><key>Alt</key><key>S</key></keyseq>.</p>
      </item>
      <item>
        <p>Digite <cmd>orca</cmd>, juntamente com os parametros opcionais, em uma janela do terminal ou dentro do diálogo <gui>Run</gui> e então pressione <key>Enter</key>.</p>
      </item>
    </list>
  </section>
    <section id="loadtime">
      <title>Opções de carregamento</title>
      <p>As seguintes opções podem ser especificadas ao executar o <app>Orca</app> em uma janela de terminal ou através do diálogo <gui>Executar</gui>:</p>
      <list>
        <item>
          <p><cmd>-h</cmd>, <cmd>--help</cmd>: Mostra mensagem de ajuda</p>
        </item>
        <item>
          <p><cmd>-v</cmd>, <cmd>--version</cmd>: Mostra a versão do <app>Orca</app></p>
        </item>
        <item>
          <p><cmd>-s</cmd>, <cmd>--setup</cmd>: Configura as preferências do usuário</p>
        </item>
        <item>
          <p><cmd>-u</cmd>, <cmd>--user-prefs=<em>nome do diretório</em></cmd>: Usa <em>nome do diretório</em> como o diretório alternativo para as preferências do usuário</p>
        </item>
        <item>
          <p><cmd>-e</cmd>, <cmd>--enable=<em>opção</em></cmd>: Força o uso de uma opção, onde a <em>opção</em> pode ser uma das seguintes:</p>
          <list>
            <item><p><cmd>speech</cmd></p></item>
            <item><p><cmd>braille</cmd></p></item>
            <item><p><cmd>braille-monitor</cmd></p></item>
          </list>
        </item>
        <item>
          <p><cmd>-d</cmd>, <cmd>--disable=<em>opção</em></cmd>: Impede o uso de uma opção, onde a <em>opção</em> pode ser um dos seguintes:</p>
          <list>
            <item><p><cmd>speech</cmd></p></item>
            <item><p><cmd>braille</cmd></p></item>
            <item><p><cmd>braille-monitor</cmd></p></item>
          </list>
        </item>
        <item>
            <p><cmd>-p</cmd>, <cmd>--profile=<em>nome do arquivo</em></cmd>: Importa um perfil de um determinado arquivo de perfil do <app>Orca</app></p>
        </item>
        <item>
          <p><cmd>-r</cmd>, <cmd>--replace</cmd>: Substitui uma instância em execução do <app>Orca</app></p>
        </item>
        <item>
          <p><cmd>-l</cmd>, <cmd>--list-apps</cmd>: Lista os aplicativos conhecidos que estão sendo executados</p>
        </item>
        <item>
          <p><cmd>--debug</cmd>: Envia a saída de depuração para debug-AAAA-MM-DD-HH:MM:SS.out</p>
        </item>
        <item>
          <p><cmd>--debug-file=<em>nome do arquivo</em></cmd>: Envia a saída de depuração para o arquivo especificado</p>
        </item>
      </list>
    </section>

</page>
