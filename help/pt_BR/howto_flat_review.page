<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_flat_review" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#reviewing"/>
    <link type="next" xref="howto_orca_find"/>
    <title type="sort">2. Revisão Plana</title>
    <desc>Examinando uma janela espacialmente</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Revisão Plana</title>
  <p>O recurso Revisão Plana do <app>Orca</app> permite revisar espacialmente o conteúdo, tanto texto quanto widgets, da janela ativa. Nesse modo, o <app>Orca</app> trata a janela como se fosse uma folha de texto bidimensional, eliminando qualquer noção de hierarquia de widgets ou outro agrupamento lógico dentro da janela.</p>
  <p>O conteúdo "plano", também conhecido como revisão plana, pode ser <link xref="commands_flat_review">navegado</link> por linha, por palavra, por caractere e por objeto. Além disto, você pode executar um clique esquerdo ou um clique direito do mouse no objeto que está sendo revisado. Finalmente, você pode usar a <link xref="howto_orca_find">Localização do Orca</link>, um recurso baseado na revisão plana para pesquisar o conteúdo da janela ativa.</p>
  <p>Como o contexto da Revisão Plana é uma representação espacial do conteúdo da janela ativa, ele é criado quando você entra na Revisão Plana e contém apenas os objetos que são visíveis. Como resultado, você não poderá usar a Revisão Plana para acessar itens que estão na janela, mas atualmente fora da tela. Além disso, se o conteúdo da janela mudar por conta própria, o contexto da Revisão Plana não será atualizado automaticamente. Você pode fazer com que um novo contexto seja construído reiniciando a Revisão Plana.</p>
  <p>Por fim, a Revisão Plana, por sua natureza, é um modo que não pode ser usado ao mesmo tempo em que o <app>Orca</app> está rastreando o foco. Assim, se você estiver na Revisão Plana e usar os comandos de navegação do aplicativo para mover o cursor ou para dar foco a outro objeto, sairá automaticamente da Revisão Plana.</p>
</page>
