<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_speech" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_braille"/>
    <title type="sort">2. Fala</title>
    <title type="link">Fala</title>
    <desc>Configurando o que é falado</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Preferências de Fala</title>
  <section id="enable_speech">
    <title>Habilitar fala</title>
    <p>A caixa de seleção <gui>Habilitar fala</gui> controla se o <app>Orca</app> deve ou não fazer uso de um sintetizador de voz. Usuários que usam apenas Braille provavelmente desejarão desmarcar esta opção.</p>
    <p>Valor padrão: selecionado</p>
  </section>
  <section id="verbosity">
    <title>Verbosidade</title>
    <p>A configuração de <gui>Verbosidade</gui> determina a quantidade de informação que será falada em diversas situações. Por exemplo, se for ajustado para detalhado e você se deslocar com as teclas direcionais sobre uma palavra que está incorretamente soletrada, o <app>Orca</app> dirá que ela está incorretamente soletrada. Quando o nível é ajustado para breve, este anúncio não será feito.</p>
    <p>Valor padrão: <gui>Detalhado</gui></p>
  </section>
  <section id="punctuation_level">
    <title>Nível de pontuação</title>
    <p>O botão de opção <gui>nível de pontuação</gui> é usado para ajustar a quantidade de pontuação falada pelo sintetizador. Os níveis disponíveis são <gui>Nenhum</gui>, <gui>Algum</gui>, <gui>Maioria</gui>, e <gui>Todas</gui>.</p>
    <p>Valor padrão: <gui>A maioria</gui></p>
    <section id="punctuation_level_none">
      <title>Nenhum</title>
      <p>Escolher um nível de pontuação <gui>Nenhum</gui>, como esperado, fará com que nenhuma pontuação seja dita. Observe, no entanto, que símbolos especiais, como números subscritos e sobrescritos, frações Unicode e marcadores, ainda serão falados nesse nível, embora alguns possam considerar esses tipos de símbolos de pontuação.</p>
    </section>
    <section id="punctuation_level_some">
      <title>Algum</title>
      <p>Escolher um nível de pontuação <gui>Algum</gui> faz com que todos os símbolos mencionados anteriormente sejam falados. Além disso, o <app>Orca</app> falará símbolos matemáticos conhecidos, símbolos de moeda e "^", "@", "/", "&amp;", "#".</p>
    </section>
    <section id="punctuation_level_most">
      <title>Maioria</title>
      <p>Escolher um nível de pontuação <gui>Maioria</gui> faz com que todos os símbolos mencionados anteriormente sejam falados. Além disso, o <app>Orca</app> falará todos os outros símbolos de pontuação conhecidos, <em>exceto</em> "!", "'", ","", ".", "?".</p>
    </section>
    <section id="punctuation_level_all">
      <title>Tudo</title>
      <p>Escolher um nível de pontuação <gui>Tudo</gui>, como esperado, faz com que o <app>Orca</app> fale todos os símbolos de pontuação conhecidos.</p>
    </section>
  </section>
  <section id="spoken_context">
    <title>Contexto Falado</title>
    <p>Os itens seguintes controlam a apresentação de uma variedade de informações de "sistema" sobre o item com foco. Já que o texto associado não aparece na tela, esta informação é apresentada na "voz de sistema" do <app>Orca</app>.</p>
    <section id="only_speak_displayed_text">
      <title>Falar somente o texto exibido</title>
      <p>Marcar esta caixa de seleção faz com que o <app>Orca</app> fale apenas o texto real exibido na tela. Esta opção destina-se principalmente a usuários com baixa visão e usuários com deficiência visual.</p>
      <p>Valor padrão: não selecionado</p>
      <note style="note">
        <p>Os itens a seguir não estarão disponíveis para configuração se a caixa de seleção <gui>Somente falar o texto exibido</gui> estiver marcada.</p>
      </note>
    </section>
    <section id="speak_blank_lines">
      <title>Falar as linhas em branco</title>
      <p>Se a caixa de seleção <gui>Falar linhas em branco</gui> estiver marcada, o <app>Orca</app> dirá "em branco" toda vez que você apontar para uma linha em branco. Se estiver desmarcada, o <app>Orca</app> não dirá nada.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="indentation_and_justification">
      <title>Falar recuo e justificação</title>
      <p>Quando trabalhamos com código ou editamos outros documentos, muitas vezes é desejável ter informações de justificação e de recuo. Marcando a caixa de seleção <gui>Falar recuo e justificação</gui> fará com que o <app>Orca</app> anuncie essa informação.</p>
      <p>Valor padrão: não selecionado</p>
    </section>
    <section id="speak_misspelled_word_indicator">
      <title>Falar indicador de palavra com erro de ortografia</title>
      <p>O indicador de palavras com erros ortográficos é a linha ondulada vermelha que aparece abaixo das palavras com erros ortográficos em campos de texto editáveis. Se <gui>Falar indicador de palavra com erro de ortografia</gui> estiver marcado, quando você navegar em uma palavra com este indicador ou digitar uma palavra incorretamente fazendo com que este indicador apareça, o <app>Orca</app> falará "errado."</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="mnemonics">
     <title>Falar mnemônicos do objeto</title>
      <p>Se a caixa de seleção <gui>Falar mnemônicos do objeto</gui> estiver marcada, o <app>Orca</app> anunciará o mnemônico associado ao objeto em foco (como <keyseq><key>Alt</key><key>O</key> </keyseq> para um botão <gui>OK</gui>).</p>
      <p>Valor padrão: não selecionado</p>
    </section>
    <section id="child_position">
      <title>Fala posição filha</title>
      <p>Marcar a caixa de seleção <gui>Falar posição filha</gui> fará com que o <app>Orca</app> anuncie a posição do item em foco em menus, listas e árvores (por exemplo, "9 de 16").</p>
      <p>Valor padrão: não selecionado</p>
    </section>
    <section id="speak_tutorial_messages">
      <title>Falar mensagens tutorial</title>
      <p>Se a caixa de seleção <gui>Falar mensagens tutorial</gui> estiver marcada, conforme você se move entre os objetos em uma interface, o <app>Orca</app> fornecerá informações adicionais, como como interagir com o objeto em foco no momento.</p>
      <p>Valor padrão: não selecionado</p>
    </section>
    <section id="speak_description">
      <title>Falar descrição</title>
      <p>Se a caixa de seleção <gui>Falar descrição</gui> estiver marcada, conforme você se move entre os objetos em uma interface, o <app>Orca</app> falará a descrição acessível além do nome acessível do objeto.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="system_messages_are_detailed">
      <title>Detalhar mensagens do sistema</title>
      <p>Se a opção <gui>Detalhar mensagens do sistema</gui> estiver marcada, o <app>Orca</app> apresentará mensagens detalhadas para você em voz. Por exemplo, se você usar o comando do <app>Orca</app> para alterar o eco, o <app>Orca</app> pode falar "Eco definido como palavra". Se você preferir mensagens mais curtas, como simplesmente "palavra", desmarque essa caixa de seleção.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="speak_colors_as_names">
      <title>Fale cores como nomes</title>
      <p>Se <gui>Falar cores como nomes</gui> estiver marcado, o <app>Orca</app> descreverá as cores, procurando a aproximação mais próxima. Por exemplo, RGB 0, 27, 51 seria falado como "azul da meia-noite". Se você preferir ouvir o valor RGB exato, desmarque esta caixa de seleção.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="announce_blockquotes_during_navigation">
      <title>Anunciar citações durante a navegação</title>
      <p>Se a opção <gui>Anunciar citações durante a navegação</gui> estiver marcada, o <app>Orca</app> informará quando você entrar ou sair de uma bloco de citação. Observe que essa configuração é independente de esse anúncio ser feito ou não durante o Ler Tudo. Consulte <link xref="preferences_general#say_all_announce_context">Anunciar informações contextuais em Ler Tudo</link> para obter mais informações.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="announce_forms_during_navigation">
      <title>Anunciar formulários durante a navegação</title>
      <p>Se a opção <gui>Anunciar formulários durante a navegação</gui> estiver marcada, o <app>Orca</app> informará quando você entrar ou sair de um formulário. Observe que essa configuração é específica para formulários que não são pontos de referência ARIA. Você pode configurar a apresentação de pontos de referência ARIA através da caixa de seleção <gui>Anunciar marcos durante a navegação</gui>. Além disso, observe que essa configuração é independente de esse anúncio ser feito ou não durante o Ler Tudo. Consulte <link xref="preferences_general#say_all_announce_context">Anunciar informações contextuais em Ler Tudo</link> para obter mais informações.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="announce_landmarks_during_navigation">
      <title>Anuncie pontos de referência durante a navegação</title>
      <p>Se <gui>Anunciar marcos durante a navegação</gui> estiver marcado, o <app>Orca</app> informará quando você entrar ou sair de um ponto de referência ARIA. Observe que essa configuração é independente de esse anúncio ser feito ou não durante o Ler Tudo. Consulte <link xref="preferences_general#say_all_announce_context">Anunciar informações contextuais em Ler Tudo</link> para obter mais informações.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="announce_lists_during_navigation">
      <title>Anunciar listas durante a navegação</title>
      <p>Se a opção <gui>Anunciar listas durante a navegação</gui> estiver marcada, o <app>Orca</app> informará quando você entrar ou sair de uma lista. Observe que essa configuração é independente de esse anúncio ser feito ou não durante o Ler Tudo. Consulte <link xref="preferences_general#say_all_announce_context">Anunciar informações contextuais em Ler Tudo</link> para obter mais informações.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="announce_panels_during_navigation">
      <title>Anunciar painéis durante a navegação</title>
      <p>Se a opção <gui>Anunciar painéis durante a navegação</gui> estiver marcada, o <app>Orca</app> informará quando você entrar ou sair de um painel. Observe que essa configuração é independente de esse anúncio ser feito ou não durante o Ler Tudo. Consulte <link xref="preferences_general#say_all_announce_context">Anunciar informações contextuais em Ler Tudo</link> para obter mais informações.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="announce_tables_during_navigation">
      <title>Anunciar tabelas durante a navegação</title>
      <p>Se a opção <gui>Anunciar tabelas durante a navegação</gui> estiver marcada, o <app>Orca</app> informará quando você entrar ou sair de uma tabela. Observe que essa configuração é independente de esse anúncio ser feito ou não durante o Ler Tudo. Consulte <link xref="preferences_general#say_all_announce_context">Anunciar informações contextuais em Ler Tudo</link> para obter mais informações.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="speak_full_row_in_gui_tables">
      <title>Falar linha completa em tabelas da interface gráfica</title>
      <p>Se a opção <gui>Falar linha completa nas tabelas da interface gráfica</gui> estiver marcada, conforme você move a seta para cima e para baixo nas tabelas do aplicativo, como a lista de mensagens em sua caixa de entrada, o <app>Orca</app> falará a linha inteira. Se preferir ouvir apenas a célula com foco, desmarque esta caixa de seleção.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="speak_full_row_in_document_tables">
      <title>Falar linha completa em tabelas de documentos</title>
      <p>Se a opção <gui>Falar linha completa em tabelas de documentos</gui> estiver marcada, conforme você direciona a seta para cima e para baixo em tabelas como as encontradas no <app>Writer</app> e documentos da Web, o <app>Orca</app> vai falar a linha inteira. Se preferir ouvir apenas a célula com foco, desmarque esta caixa de seleção.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="speak_full_row_in_spreadsheets">
      <title>Falar linha completa em planilhas</title>
      <p>Se a opção <gui>Falar linha completa em planilhas</gui> estiver marcada, conforme você move as setas para cima e para baixo nas planilhas, o <app>Orca</app> falará a linha inteira. Se preferir ouvir apenas a célula com foco, desmarque esta caixa de seleção.</p>
      <p>Valor padrão: não selecionado</p>
    </section>
  </section>
</page>
